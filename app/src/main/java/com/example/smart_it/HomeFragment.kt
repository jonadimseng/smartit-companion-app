package com.example.smart_it

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.smart_it.databinding.FragmentHomeBinding
import org.eclipse.paho.client.mqttv3.MqttClient

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var mqttClient: MQTTClient
    private lateinit var tinyDB: TinyDB
    private lateinit var hideCamera: MenuItem //so camera is not displayed in options
    private lateinit var brokerURI: String
    private var clientId: String =
        MqttClient.generateClientId() //clientID for broker is generated here

    //hardcoded topics
    private var topicPower: String = "cmnd/smartplug/POWER1"
    private var topicUSB: String = "cmnd/smartplug/POWER2"
    private var topicLight: String = "cmnd/smartlight/Power"
    private var topicColor: String = "cmnd/smartlight/Color1"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tinyDB = TinyDB(context)
        brokerURI = tinyDB.getString("brokerURI")
        Log.d("HomeFragment", brokerURI)

        fun switchListener(switchName: SwitchCompat) {
            // Holt sich den Namen einer View und added einen OnClickListener (das geht sicherlich einfacher/effizienter)
            when (switchName.toString().substringAfter("id/").dropLast(1)) {
                "switchPower" -> {
                    switchName.setOnClickListener {
                        if (mqttClient.isConnected()) {
                            if (switchName.isChecked) {
                                mqttClient.publish(topicPower, "ON", 1, true)
                            } else {
                                mqttClient.publish(topicPower, "OFF", 1, true)
                            }
                        }
                    }
                }
                "switchUSB" -> {
                    switchName.setOnClickListener {
                        if (mqttClient.isConnected()) {
                            if (switchName.isChecked) {
                                mqttClient.publish(topicUSB, "ON", 1, true)
                            } else {
                                mqttClient.publish(topicUSB, "OFF", 1, true)
                            }
                        }
                    }
                }
                "switchLight" -> {
                    switchName.setOnClickListener {
                        if (mqttClient.isConnected()) {
                            if (switchName.isChecked) {
                                mqttClient.publish(topicLight, "ON", 1, true)
                                binding.currentColor.isVisible = true
                            } else {
                                mqttClient.publish(topicLight, "OFF", 1, true)
                                binding.currentColor.isVisible = false
                            }
                        }
                    }
                }
            }
        }

        binding.switchPower.isEnabled = false
        binding.switchUSB.isEnabled = false
        binding.switchLight.isEnabled = false

        switchListener(binding.switchPower)
        switchListener(binding.switchUSB)
        switchListener(binding.switchLight)

        mqttClient = MQTTClient(context, brokerURI, clientId)
        mqttClient.connect()
        Log.d("HomeFragment", "MQTT-Broker successfully connected")

        //observes new MQTT Messages
        mqttClient.messagePayload.observeForever { payLoad ->
            when (mqttClient.receivedTopic) {
                topicPower -> {
                    binding.switchPower.isChecked = mqttClient.receivedMessage == "ON"
                    binding.switchPower.isEnabled = true
                }
                topicLight -> {
                    //legacy code if something breaks
                    /*binding.switchLight.isChecked = mqttClient.receivedMessage == "ON"
                    binding.switchLight.isEnabled = true*/
                    binding.switchLight.isEnabled = true
                    if (mqttClient.receivedMessage == "ON") {
                        binding.switchLight.isChecked = true
                        binding.currentColor.isVisible = true
                    } else {
                        binding.switchLight.isChecked = false
                        binding.currentColor.isVisible = false
                    }

                }
                topicUSB -> {
                    binding.switchUSB.isChecked = mqttClient.receivedMessage == "ON"
                    binding.switchUSB.isEnabled = true
                }
                topicColor -> {
                    when (mqttClient.receivedMessage) {
                        "1" -> binding.currentColor.setBackgroundColor(resources.getColor(R.color.red))
                        "2" -> binding.currentColor.setBackgroundColor(resources.getColor(R.color.green))
                        "3" -> binding.currentColor.setBackgroundColor(resources.getColor(R.color.blue))
                        "12" -> binding.currentColor.setBackgroundColor(0)
                    }
                    if (binding.switchLight.isChecked) {
                        binding.currentColor.isVisible = true
                    }
                }
            }
        }
        binding.lightIcon.setOnClickListener {
            val popupMenu: PopupMenu = PopupMenu(context, binding.lightIcon)
            popupMenu.menuInflater.inflate(R.menu.color_picker_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.rgb_red ->
                        if (mqttClient.isConnected() && binding.switchLight.isChecked) {
                            mqttClient.publish(topicColor, "1", 1, true)
                        } else {
                            Toast.makeText(
                                context,
                                "Not possible. Light is off",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    R.id.rgb_green ->
                        if (mqttClient.isConnected() && binding.switchLight.isChecked) {
                            mqttClient.publish(topicColor, "2", 1, true)
                        } else {
                            Toast.makeText(
                                context,
                                "Not possible. Light is off",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    R.id.rgb_blue ->
                        if (mqttClient.isConnected() && binding.switchLight.isChecked) {
                            mqttClient.publish(topicColor, "3", 1, true)
                        } else {
                            Toast.makeText(
                                context,
                                "Not possible. Light is off",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    R.id.rgb_white ->
                        if (mqttClient.isConnected() && binding.switchLight.isChecked) {
                            mqttClient.publish(topicColor, "12", 1, true)
                        } else {
                            Toast.makeText(
                                context,
                                "Not possible. Light is off",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                }
                true
            }
            if (mqttClient.isConnected() && binding.switchLight.isChecked) {
                popupMenu.show()
            } else {
                Toast.makeText(context, "Not possible. Light is off", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.navdrawer_menu, menu)
        inflater.inflate(R.menu.options_menu, menu)
        hideCamera = menu.findItem(R.id.cameraFragment)
        hideCamera.isVisible = false
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            requireView().findNavController()
        ) || super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (mqttClient.isConnected()) {
            mqttClient.disconnect()
        }
    }
}