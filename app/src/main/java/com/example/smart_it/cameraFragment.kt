package com.example.smart_it

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.smart_it.databinding.FragmentCameraBinding
import com.longdo.mjpegviewer.MjpegView

class CameraFragment : Fragment() {
    private lateinit var tinyDB: TinyDB
    private lateinit var binding: FragmentCameraBinding
    private lateinit var cameraURI: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_camera, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tinyDB = TinyDB(context)
        cameraURI = tinyDB.getString("cameraURI")
        Log.d("CameraFragment", cameraURI)

        val viewer = binding.mjpegview
        viewer.mode = MjpegView.MODE_FIT_WIDTH
        viewer.isAdjustHeight = true
        viewer.supportPinchZoomAndPan = true
        viewer.setUrl(cameraURI)
        viewer.startStream()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val viewer = binding.mjpegview
        viewer.stopStream()
    }
}