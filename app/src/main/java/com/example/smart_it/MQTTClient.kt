package com.example.smart_it

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*


class MQTTClient(context: Context?, serverURI: String, clientID: String) {

    private var mqttClient = MqttAndroidClient(context, serverURI, clientID)
    var receivedTopic: String = "empty"
    var receivedMessage: String = "empty"
    val messagePayload = MutableLiveData<String>()

    //connect mqtt broker
    fun connect() {
        val options = MqttConnectOptions()

        mqttClient.setCallback(object : MqttCallback {
            //lost connection to broker
            override fun connectionLost(cause: Throwable?) {
                Log.d("MqttClient", "Connection lost ${cause.toString()}")
            }

            //receive new message from broker
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                receivedMessage = message.toString()
                receivedTopic = topic.toString()
                messagePayload.value = receivedMessage
                Log.d("MqttClient", "$receivedMessage FROM $receivedTopic")
            }

            //complete message delivery to broker
            override fun deliveryComplete(token: IMqttDeliveryToken?) {
                Log.d("MqttClient", "Delivery completed")
            }
        })

        try {
            mqttClient.connect(options, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("MqttClient", "Connection successful")
                    Log.i("MqttClient", "Client ID = ${mqttClient.clientId}")
                    //this is hardcoded. but actually shouldnt be. welp.
                    mqttClient.subscribe("cmnd/smartplug/POWER1", 1)
                    mqttClient.subscribe("cmnd/smartplug/POWER2", 1)
                    mqttClient.subscribe("cmnd/smartlight/Power", 1)
                    mqttClient.subscribe("cmnd/smartlight/Color1", 1)
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("MqttClientConnect", "Connection failed")
                }
            })

        } catch (e: MqttException) {
            Log.d("MQTTClient", "ERROR")
            e.printStackTrace()
        }
    }

    fun isConnected(): Boolean {
        Log.d("MQTTClientConnect", "Connection: ${mqttClient.isConnected}")
        return mqttClient.isConnected
    }

    fun subscribe(topic: String, qos: Int = 1) {
        try {
            mqttClient.subscribe(topic, qos, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("MqttClient", "Subscribed to $topic")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("MqttClient", "Subscription to $topic failed")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun unsubscribe(topic: String) {
        try {
            mqttClient.unsubscribe(topic, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("MqttClient", "Unsubscribed from $topic")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("MqttClient", "Failed to unsubscribe from $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    // Retain flag:
    // defines if the message has to be saved by the broker as the last known good value for the topic.
    // When a new client will subscribe to this topic, it receives the last retained message.
    fun publish(topic: String, msg: String, qos: Int = 1, retained: Boolean = true) {
        try {
            val message = MqttMessage()
            message.payload = msg.toByteArray()
            message.qos = qos
            message.isRetained = retained
            mqttClient.publish(topic, message, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("MqttClient", "$msg published to $topic")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("MqttClient", "Failed to publish $msg to $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun disconnect() {
        try {
            mqttClient.disconnect(null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("MqttClient", "Disconnected")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("MqttClient", "Failed to disconnect")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }
}