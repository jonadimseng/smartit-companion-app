package com.example.smart_it

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SwitchCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.smart_it.databinding.FragmentSettingBinding

class SettingFragment : Fragment() {

    private lateinit var binding: FragmentSettingBinding
    private lateinit var cameraURI: String
    private lateinit var brokerURI: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val tinyDB = TinyDB(context)
        val btn: SwitchCompat = binding.darkmodetoggle

        //check for light or darkmode preferences and change switch
        when (resources.configuration.uiMode.and(Configuration.UI_MODE_NIGHT_MASK)) {
            Configuration.UI_MODE_NIGHT_NO -> btn.isChecked = false
            Configuration.UI_MODE_NIGHT_YES -> btn.isChecked = true
        }

        btn.setOnCheckedChangeListener { _, isChecked ->
            when (resources.configuration.uiMode.and(Configuration.UI_MODE_NIGHT_MASK)) {
                Configuration.UI_MODE_NIGHT_NO -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                    tinyDB.putBoolean("darkmode", true)
                }
                Configuration.UI_MODE_NIGHT_YES -> {

                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    tinyDB.putBoolean("darkmode", false)
                }
            }
        }

        binding.confirmSettings.setOnClickListener {
            val broker = binding.brokerInput.text.toString()
            val camera = binding.cameraInput.text.toString()

            if (broker.trim().isNotEmpty()) {
                tinyDB.putString("brokerURI", broker)
            } else {
                tinyDB.putString("brokerURI", "tcp://77.237.53.201:21883")
                //tinyDB.putString("brokerURI", "tcp://broker.emqx.io:1883")
            }

            if (camera.trim().isNotEmpty()) {
                tinyDB.putString("cameraURI", camera)
            } else {
                tinyDB.putString("cameraURI", "http://77.237.53.201:28000/stream.mjpg")
                //tinyDB.putString("cameraURI", "http://82.161.213.116:50000/mjpg/video.mjpg")
            }

            cameraURI = tinyDB.getString("cameraURI")
            brokerURI = tinyDB.getString("brokerURI")
            Log.d("SettingFragment", "cameraURI: $cameraURI")
            Log.d("SettingFragment", "brokerURI: $brokerURI")
        }
    }
}