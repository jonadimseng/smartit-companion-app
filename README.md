# Smart-It Companion App

## About The Project

<img src="https://i.imgur.com/K015W8X.png" align="right" alt="Screenshot" width="200"/>

The Smart-It app serves as a control interface for the private smart home system and allows the user to turn the lamp and two sockets on and off. In addition, the mobile app supports a live feed from the camera to watch the door in front of the home.

### Built With

* [Kotlin](https://kotlinlang.org/)
* [Android Studio](https://developer.android.com/studio/)
* [TinyDB](https://github.com/kcochibili/TinyDB--Android-Shared-Preferences-Turbo)
* [android-mpjeg-view](https://github.com/perthcpe23/android-mjpeg-view)
* [Eclipse Paho (MQTT)](http://www.eclipse.org/paho/)


## Usage

### Home

When you start the app, you are in the Home menu. This home menu offers a control interface and shows which devices are currently switched on and which are switched off.

By tapping on the switch-button on the right side, the device can be turned on or off.
If a device is switched on, this is indicated by the greenish color of the switch.
If a device is switched off, this is indicated by the reddish color of the switch.

The lamp can be set to the following colours:
* Red
* Blue
* Green
* White (here, a symbol will be shown instead of a colour)

In addition, it is possible to switch to the live camera transmission tab using the sidedrawer.
The three dots at the top right of the screen can be used to switch to the settings.

### Camera

Here it is possible to see a live transmission of the camera.
The camera URI can be changed individually in the settings, so that a different live stream can be seen.

### Settings

Here it is possible to set the BrokerURI and the CameraURI by entering the exact URI including the port in the text field. By clicking on the Confirm button these changes will be applied immediately.

A BrokerURI can look like this:
```
tcp://broker.emqx.io:1883
```
A CameraURI can look like this:
```
http://82.161.213.116:50000/mjpg/video.mjpg
```

If the Confirm button is clicked without specifying anything in the text field, the BrokerURI resets to the default, which is:
```tcp://77.237.53.201:21883```
The same is true for the cameraURI, where the default is:
```http://77.237.53.201:28000/stream.mjpg```

It is also possible to switch between dark mode and light mode here via a switch.

## Made by
* Elna Halilovic
* Jonathan Dimmel-Sengmüller

## Acknowledgements
* [FH-Campus Wien](https://www.fh-campuswien.ac.at/)
* [Stackoverflow](https://stackoverflow.com/)
* [Android Documentation](https://developer.android.com/docs/)
